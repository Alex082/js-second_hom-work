/*
1. Які існують типи даних у Javascript?

Undefined (непроницаемый), 
Null (нулевой), 
Boolean (логичный), 
String (строчный), 
Number (чисельный), 
Object (обьектный).

2. У чому різниця між == і ===?

Оператор ( == ) сравнивает на равенство двух значений.

Оператор ( === ) сравнивает на едентичность.

3. Що таке оператор?

Конструкция ( оператор ) позволяет контролировать следует
 ли программе выполнять определенную часть кода или нет в
 зависимости от того, какое данное условие — истинным или ложным.
*/


// Используем prompt чтобы отобразить имя и возраст 
let userName = ''; 
let userAge = ''; 
 
while (true) { 
  userName = prompt('What is your name?'); 
  if (!userName.match(/^[A-Za-z]+$/)) { 
    alert('Введи правильно свое имя'); 
    continue; 
  } 
  break;
}
while (true) {
  userAge = prompt('What is your age?'); 
  if (!userAge.match(/^[0-9]+$/)) { 
    alert('Введи правильно свой возраст'); 
    continue; 
  } 
  break; 
} 
userAge = Number(userAge);

// Отображаем в alert box сообщения 
/* alert("Your name is " + name + " and you are " + age + " years old."); */ 
 
// Проверяем, если меньше 18, то не разрешаем войти на сайт и пишем сообщение 
if (userAge < 18) { 
    alert("You are not allowed to visit this website."); 
} else if (userAge >= 18 && userAge <= 22) { 
  // Если возраст от 18 до 22 (включительно, то показываем окна с сообщением 
  // и добавляем кнопку "ок" 
  let confirmEntry = confirm("Are you sure you want to continue?"); 
  if (confirmEntry) { 
    // Если пользователь нажал кнопку "ок", то на экране показываем сообщение и имя 
    let welcomeMessage = "Welcome, " + userName + " "; 
    alert(welcomeMessage); 
  } else { 
    // Если нажимает отмена, то показываем сообщение, что он не может аойти на этот сайт 
    alert("You are not allowed to visit this website."); 
  } 
} else { 
  // Если пользователь больше 22 лет нажал кнопку "ок", то на экране показываем сообщение и имя 
  let welcomeMessage = "Welcome, " + userName + " "; 
  alert(welcomeMessage); 
}